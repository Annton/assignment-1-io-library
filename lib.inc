%define WRITE_SYSCALL 1
%define STDOUT 1
%define STDIN 0
%define EXIT_SYSCALL 60
%define READ_SYSCALL 0
section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT_SYSCALL
    syscall


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor     rax, rax
    .loop:
        cmp byte[rdi+rax], 0
        je .end_loop
        inc rax
        jmp .loop
    .end_loop:
        ret    


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, WRITE_SYSCALL
    mov rdi, STDOUT
    syscall
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`   
    


; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, WRITE_SYSCALL
    mov rdi, STDOUT
    mov rsi, rsp
    mov rdx, 1
    syscall 
    pop rdi
    ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns print_uint
    push rdi
    mov rdi, `-`
    call print_char
    pop rdi
    neg rdi


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:


    
    mov r8, rsp; 
    push 0; 
    mov rax, rdi; 
    mov r10,10

    .loop:
        xor rdx, rdx
        div r10
        add dl, 0x30; из цифры получаем её код ASCII
        dec rsp
        mov [rsp], dl; 
        test rax, rax; 
        jnz .loop
        
    .finish:
    mov rdi, rsp
    push r8
    call print_string
    pop rsp
    ret




; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    .loop:
        
        mov dl, [rsi+rax]
        cmp dl, [rdi+rax]
        jne .not_equals
        test dl, dl
        jz .equals
        inc rax
        jmp .loop
    .not_equals:
        xor rax, rax
        ret

    .equals:
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rax, READ_SYSCALL
    mov rdi, STDIN
    mov rdx, 1
    mov rsi, rsp
    syscall
    pop rax
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
; rdi - адрес буфера
; rsi - размер
read_word:
    

    push r12
    push r13
    push r14
    mov r14, rdi
    mov r12, rsi
    dec r12

    .skip_spaces:
        call read_char
        test rax, rax
        jz .finish
        cmp rax, ` `
        je .skip_spaces
        cmp rax, `\t`
        je .skip_spaces
        cmp rax, `\n`
        je .skip_spaces
    
    .loop:
        cmp r12, r13
        jz .too_small_buffer
        mov byte[r13+r14], al
        inc r13
        call read_char
        test rax, rax
        jz .finish
        cmp rax, ` `
        je .finish
        cmp rax, `\t`
        je .finish
        cmp rax, `\n`
        je .finish

        jmp .loop

    .too_small_buffer:
        xor rax, rax
        pop r14
        pop r13
        pop r12
        ret

    .finish:
        mov rax, r14
        mov rdx, r13
        mov byte[r14+r13], 0
        pop r14
        pop r13
        pop r12
        ret



; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor rcx, rcx
    .loop:
        mov dl, byte[rdi+rcx]
        sub dl, '0'
        jl .finish
        cmp dl, 9
        jg .finish
        inc rcx
        
        
        imul rax, rax, 10
        add rax, rdx
        jmp .loop
    


    .finish:
        mov rdx, rcx
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx
    xor r10, r10
    mov r10b, byte[rdi]
    cmp r10b, '-'
    je .minus
    cmp r10b, '+'
    je .plus
    jmp .no_sign
    
    .minus:
        inc rdi
        call parse_uint
        inc rdx
        neg rax
        ret
    .plus:
        inc rdi
        call parse_uint
        inc rdx
        ret
    .no_sign:
        jmp parse_uint

    
    

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; rdi - указаель на строку
; rsi - указатель на буфер
; rdx - длина буффера
string_copy:
    
    xor rax, rax
    .loop:
        cmp rdx, rax
        jl .too_long_stroka
        mov r10b, byte [rdi+rax]
        mov byte[rsi+rax], r10b
        test r10b, r10b
        jz .finish
        inc rax
        jmp .loop

        
    .too_long_stroka:
        xor rax, rax
        ret
    .finish:
        ret


